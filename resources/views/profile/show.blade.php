@extends('layouts.app')

@section('title')
    Profile
@endsection

@section('content')
<table class="table table-hover text-nowrap">
    <thead>
      <tr>
        <th>Full Name</th>
        <th>Bio</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($query as $key => $profile)
        <tr>
          <td> {{ $profile->full_name }} </td>
          <td> {{ $profile->bio }} </td>
          <td class="d-flex inline">
            <a href="/profile/{{ $profile->id }}/edit"><button class="btn btn-sm">Edit</button></a>
            <form action="/cast/{{ $profile->id }}" method="POST">
              @csrf
              @method('DELETE')
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="3" align="center"> No data, create profile <a href="/profile/create">here</a></td>
        </tr>
      @endforelse
    </tbody>
  </table>

@endsection