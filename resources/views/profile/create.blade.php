@extends('layouts.app')

@section('title')
    Create Profile
@endsection

@section('content')
<form action="/profile" method="POST">
    @csrf
    @method('POST')
    <div class="card-body">
        <div class="form-group">
            <label for="fullname">Full Name</label>
            <input type="text" class="form-control" id="fullname" name="fullname" value="{{ old('fullname', '') }}" placeholder="Enter Full Name">
            @error('fullname')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', '') }}" placeholder="Enter Bio">
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-primary" style="margin-right: 1.5rem">Create</button>
    </div>
</form>

@endsection